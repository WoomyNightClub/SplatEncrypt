# SplatEncrypt
## Another program by MCMiners9, [click here](https://github.com/MCMiners9/SplatEncrypt/releases) to download it.

## SplatEncrypt Download Availability
### SplatEncrypt is currently `AVAILABLE` for download. `Last updated: 8/8/2016 by MCMiners9`

## What is SplatEncrypt?
### SplatEncrypt has nothing to do with Splatoon. The word Splat is there just because I wanted it to be there. But basically, SplatEncrypt is a file encryptor/decryptor and text encryptor/decryptor.

## How do I get SplatEncrypt?
### Click the link above, and download the EXE file. I don't see a Mac version of SplatEncrypt coming anytime soon, sorry Mac users.

## Why are there so many files in this repository?
### That, my noob, is called "source code". It's here because if someone wants to download it and make changes to their personal copy of SplatEncrypt, they can.

## Screenshots
![SplatEncrypt](http://i.imgur.com/b1OoPD3.png)

